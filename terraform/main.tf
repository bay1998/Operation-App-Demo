variable "aws_id" {}

variable "project" {
  type    = string
  default = "operation-app"
}

module "cognito" {
  source = "./cognito"

  app_name           = var.project
  user_pool_name     = "${var.project}-userpool"
  identity_pool_name = "${var.project}-identitypool"
  env                = var.project
}

module "network" {
  source                        = "./network"
  vpc_endpoints_interface       = ["ecr.dkr", "ecr.api"]
  vpc_endpoints_security_groups = module.firewall.aws_vpc_endpoint_ec2.id
  tags                          = var.project
}


module "iam" {
  source = "./iam"
  tags   = var.project
}

module "firewall" {
  source                = "./firewall"
  public_subnet_vpc_id  = module.network.aws_public_subnet.vpc_id
  private_subnet_vpc_id = module.network.aws_private_subnet.vpc_id
  alb_port              = ["80", "443"]
  ssh_port              = "10022"
  nginx_port            = "8080"
  app_port              = "3000"
  nat_port              = "3000"
  db_port               = "3305"
  redis_port            = "6379"
  private_subnet_cidr   = "192.168.1.0/24"
  tags                  = var.project
}

module "lb" {
  source                     = "./lb"
  aws_vpc                    = module.network.aws_vpc.id
  aws_public_subnet          = module.network.aws_public_subnet.id
  aws_public_subnet_dummy    = module.network.aws_public_subnet_dummy.id
  security_group             = module.firewall.aws_lb
  enable_deletion_protection = false
  tags                       = var.project
}

module "ecr" {
  source           = "./ecr"
  nginx_image_name = "nginx-${var.project}"
  app_image_name   = "app-${var.project}"
  tags             = var.project
}

module "ecs" {
  source                = "./ecs"
  nginx_name            = "nginx_container"
  nginx_port            = "8080"
  nginx_image           = "${var.aws_id}.dkr.ecr.ap-northeast-1.amazonaws.com/nginx-${var.project}:0.1.0"
  nginx_security_groups = [module.firewall.aws_nginx_ec2.id]
  subnet                = module.network.aws_private_subnet.id
  iam_role              = module.iam.app_iam_role_ecs_task_exection
  tags                  = var.project
}
