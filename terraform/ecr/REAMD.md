### Resource

#### ECR(Elastic Container Registry)とは

- フルマネージドのコンテナイメージを管理するレジストリ

#### 運用上の注意

- リポジトリに既に存在しているタグ付きイメージを不可にする運用とする。

  - `image_tag_mutability`：`IMMUTABLE`
  - タグバージョン`latest`で更新せずに、バージョン名を指定すること！

- 以下のコマンドを実行し、Docker イメージを ECR にコミットする。

```
$ aws ecr get-login-password --region ap-northeast-1 | docker login --username AWS --password-stdin [awsID].dkr.ecr.ap-northeast-1.amazonaws.com
$ docker build -t nginx-operation-app web/
$ docker tag nginx-operation-app:latest [awsID].dkr.ecr.ap-northeast-1.amazonaws.com/nginx-operation-app:0.1.0
$ docker push [awsID].dkr.ecr.ap-northeast-1.amazonaws.com/nginx-operation-app:0.1.0
```

#### Setting

- `terraform/main.tf`での設定は以下の通りである。

```
module "ecr" {
  source           = "./ecr"
  nginx_image_name = "nginx-${var.project}"
  app_image_name   = "app-${var.project}"
  tags             = var.project
}
```

| Key              | Value                                                                      |     |
| ---------------- | -------------------------------------------------------------------------- | --- |
| nginx_image_name | Nginx 用コンテナイメージ名（`docker/web/Dockerfile`をもとに作成された）    |
| app_image_name   | RailsApp 用コンテナイメージ名（`docker/api/Dockerfile`をもとに作成された） |
