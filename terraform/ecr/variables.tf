variable "tags" {}

variable "nginx_image_name" {
  type = string
}

variable "app_image_name" {
  type = string
}
