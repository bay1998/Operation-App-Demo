output "aws_lb" {
  value = aws_security_group.alb
}

output "aws_vpc_endpoint_ec2" {
  value = aws_security_group.vpc_endpoint
}

output "aws_nginx_ec2" {
  value = aws_security_group.nginx
}

output "aws_ssh_ec2" {
  value = aws_security_group.ssh
}
