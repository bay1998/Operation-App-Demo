variable "tags" {}

variable "public_subnet_vpc_id" {
  type = string
}

variable "private_subnet_vpc_id" {
  type = string
}

variable "alb_port" {
  type = list(string)
}

variable "ssh_port" {
  type = string
}

variable "nginx_port" {
  type = string
}

variable "app_port" {
  type = string
}

variable "nat_port" {
  type = string
}

variable "db_port" {
  type = string
}

variable "redis_port" {
  type = string
}

variable "private_subnet_cidr" {
  type = string
}
