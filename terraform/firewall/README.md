### Resource

#### セキュリティグループとは

- ファイアウォールとしての役割を担う。
- AWS 上のリソースに関連づけることが可能であり、リソースレベルでの通信を制御する。
- 通信状態を記録して、ステートフルな動作（発生した通信に対する応答は自動で許可）する。

#### Setting

- `terraform/main.tf`での設定は以下の通りである。

```
module "firewall" {
  source         = "./firewall"
  public_subnet  = module.network.public_subnet_vpc_id
  private_subnet = module.network.private_subnet_vpc_id
  alb_port       = ["80", "443"]
  ssh_port       = "10022"
  nginx_port     = "8080"
  app_port       = "3000"
  nat_port       = "3000"
  db_port        = "3305"
  redis_port     = "6379"
  tags           = var.project
}
```

| Key                   | Value                                                                  |               |
| --------------------- | ---------------------------------------------------------------------- | ------------- |
| public_subnet_vpc_id  | network モジュールで設定したパブリックサブネット の VPC ID             |
| private_subnet_vpc_id | network モジュールで設定したプライベートサブネット の VPC ID           |
| alb_port              | ALB に対して Internet Gateway からの通信で許可しているポート           | ["80", "443"] |
| ssh_port              | リソースに対して許可しているポート                                     | "10022"       |
| nginx_port            | Nginx コンテナ に対して ALB からの通信で許可しているポート             | "8080"        |
| app_port              | アプリ コンテナ に対して Nginx コンテナ からの通信で許可しているポート | "3000"        |
| nat_port              | NAT Gateway に対して アプリ コンテナ からの通信で許可しているポート    | "3000"        |
| db_port               | RDB に対して アプリ コンテナ からの通信で許可しているポート            | "3305"        |
| nat_port              | ElasticCache に対して アプリ コンテナ からの通信で許可しているポート   | "6379"        |
