### ALB
resource "aws_security_group" "alb" {
  name   = "alb"
  vpc_id = var.public_subnet_vpc_id

  tags = {
    Name = var.tags
  }
}

resource "aws_security_group_rule" "alb_ingress_http" {
  for_each          = toset(var.alb_port)
  security_group_id = aws_security_group.alb.id
  type              = "ingress"
  from_port         = each.key
  to_port           = each.key
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

### SSH

resource "aws_security_group" "ssh" {
  name   = "ssh"
  vpc_id = var.private_subnet_vpc_id

  tags = {
    Name = var.tags
  }
}

resource "aws_security_group_rule" "ssh_ingress" {
  type              = "ingress"
  security_group_id = aws_security_group.ssh.id
  from_port         = "22"
  to_port           = var.ssh_port
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh_egress" {
  type              = "egress"
  security_group_id = aws_security_group.ssh.id
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

### Nginx
resource "aws_security_group" "nginx" {
  name   = "nginx"
  vpc_id = var.private_subnet_vpc_id

  tags = {
    Name = var.tags
  }
}

resource "aws_security_group_rule" "nginx_ingress" {
  type                     = "ingress"
  security_group_id        = aws_security_group.nginx.id
  from_port                = var.nginx_port
  to_port                  = var.nginx_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.alb.id
}

### App
resource "aws_security_group" "app" {
  name   = "app"
  vpc_id = var.private_subnet_vpc_id

  tags = {
    Name = var.tags
  }
}

resource "aws_security_group_rule" "app_ingress" {
  type                     = "ingress"
  security_group_id        = aws_security_group.app.id
  from_port                = var.app_port
  to_port                  = var.app_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.nginx.id
}

### NAT
resource "aws_security_group" "nat" {
  name   = "nat"
  vpc_id = var.public_subnet_vpc_id

  tags = {
    Name = var.tags
  }
}

resource "aws_security_group_rule" "nat_ingress" {
  type                     = "ingress"
  security_group_id        = aws_security_group.nat.id
  from_port                = var.nat_port
  to_port                  = var.nat_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.nat.id
}

resource "aws_security_group_rule" "nat_egress" {
  type              = "egress"
  security_group_id = aws_security_group.nat.id
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

### VPC Endpoint
resource "aws_security_group" "vpc_endpoint" {
  name   = "vpc_endpoint"
  vpc_id = var.private_subnet_vpc_id

  tags = {
    Name = var.tags
  }
}

resource "aws_security_group_rule" "vpc_endpoint_ingress" {
  type              = "ingress"
  security_group_id = aws_security_group.vpc_endpoint.id
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = [var.private_subnet_cidr]
}

resource "aws_security_group_rule" "vpc_endpoint_egress" {
  type              = "egress"
  security_group_id = aws_security_group.vpc_endpoint.id
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

### DB
resource "aws_security_group" "db" {
  name   = "db"
  vpc_id = var.private_subnet_vpc_id

  tags = {
    Name = var.tags
  }
}

resource "aws_security_group_rule" "db_ingress" {
  type                     = "ingress"
  security_group_id        = aws_security_group.db.id
  from_port                = var.db_port
  to_port                  = var.db_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.app.id
}

### Redis
resource "aws_security_group" "redis" {
  name   = "redis"
  vpc_id = var.private_subnet_vpc_id

  tags = {
    Name = var.tags
  }
}

resource "aws_security_group_rule" "redis_ingress" {
  type                     = "ingress"
  security_group_id        = aws_security_group.redis.id
  from_port                = var.redis_port
  to_port                  = var.redis_port
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.app.id
}
