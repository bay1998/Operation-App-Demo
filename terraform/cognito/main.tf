variable "app_name" {}
variable "user_pool_name" {}
variable "identity_pool_name" {}
variable "env" {}
data "aws_region" "current" {}

resource "aws_cognito_user_pool" "user_pool" {
  name = "${var.user_pool_name}-${var.env}"
  auto_verified_attributes = [
    "email",
  ]

  sms_authentication_message = " 認証コードは {####} です。"

  mfa_configuration = "ON"
  software_token_mfa_configuration {
    enabled = true
  }

  admin_create_user_config {
    allow_admin_create_user_only = false

    invite_message_template {
      email_message = " ユーザー名は {username}、仮パスワードは {####} です。"
      email_subject = " 仮パスワード"
      sms_message   = " ユーザー名は {username}、仮パスワードは {####} です。"
    }
  }

  email_configuration {
    email_sending_account = "COGNITO_DEFAULT"
  }

  password_policy {
    minimum_length                   = 8
    require_lowercase                = true
    require_numbers                  = true
    require_symbols                  = true
    temporary_password_validity_days = 7
  }

  schema {
    attribute_data_type = "String"
    name                = "email"
    required            = true
  }

  schema {
    attribute_data_type      = "String"
    developer_only_attribute = false
    mutable                  = true
    name                     = "room_number"
    required                 = false

    string_attribute_constraints {
      max_length = "256"
      min_length = "1"
    }
  }

  username_configuration {
    case_sensitive = false
  }

  verification_message_template {
    default_email_option  = "CONFIRM_WITH_LINK"
    email_message         = " 検証コードは {####} です。"
    email_message_by_link = " E メールアドレスを検証するには、次のリンクをクリックしてください。{##Verify Email##} "
    email_subject         = " 検証コード"
    email_subject_by_link = " 検証リンク"
    sms_message           = " 検証コードは {####} です。"
  }

  tags = {
    Env = "dev"
  }
}

resource "aws_cognito_user_pool_domain" "user_pool_domain" {
  domain       = "${var.app_name}-user"
  user_pool_id = aws_cognito_user_pool.user_pool.id
}

resource "aws_cognito_user_pool_client" "user_pool_client" {
  allowed_oauth_flows                  = ["code"]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_scopes                 = ["email", "openid"]
  callback_urls                        = ["http://localhost:3001/auth/sign_in"]
  logout_urls                          = ["http://localhost:3001/auth/sign_out"]
  generate_secret                      = true


  explicit_auth_flows = [
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_USER_SRP_AUTH",
  ]
  name                          = "${var.app_name}-${var.env}"
  prevent_user_existence_errors = "ENABLED"

  read_attributes = [
    "email",
    "email_verified",
    "updated_at",

  ]

  refresh_token_validity = 30
  supported_identity_providers = [
    "COGNITO",
  ]
  user_pool_id = aws_cognito_user_pool.user_pool.id

  write_attributes = [
    "email",
    "updated_at",
  ]
}

resource "aws_cognito_identity_pool" "identity_pool" {
  identity_pool_name = "${var.identity_pool_name}_${var.env}"

  allow_unauthenticated_identities = false

  openid_connect_provider_arns = []
  saml_provider_arns           = []
  supported_login_providers    = {}
  tags                         = {}

  cognito_identity_providers {
    client_id               = aws_cognito_user_pool_client.user_pool_client.id
    provider_name           = "cognito-idp.${data.aws_region.current.name}.amazonaws.com/${aws_cognito_user_pool.user_pool.id}"
    server_side_token_check = false
  }
}
