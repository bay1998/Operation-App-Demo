variable "tags" {}

variable "aws_vpc" {
  type = string
}

variable "aws_public_subnet" {
  type = string
}

variable "aws_public_subnet_dummy" {
  type = string
}

variable "security_group" {
  type = string
}

variable "enable_deletion_protection" {
  type = string
}
