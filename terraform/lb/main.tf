resource "aws_lb" "alb" {
  name                       = "alb"
  load_balancer_type         = "application"
  internal                   = false
  idle_timeout               = 60
  enable_deletion_protection = var.enable_deletion_protection

  subnets = [
    var.aws_public_subnet,
    var.aws_public_subnet_dummy
  ]

  security_groups = [
    var.security_group
  ]

  #access_logs
  tags = {
    Name = "${var.tags}-LB"
  }
}

resource "aws_lb_target_group" "test_target_group" {
  name             = "target-group"
  target_type      = "instance"
  protocol_version = "HTTP1"
  port             = 80
  protocol         = "HTTP"

  vpc_id = var.aws_vpc

  health_check {
    interval            = 30
    path                = "/"
    port                = "traffic-port"
    protocol            = "HTTP"
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
    matcher             = "200,301"
  }

  tags = {
    Name = "${var.tags}-LB"
  }
}

resource "aws_lb_listener" "test_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.test_target_group.arn
  }
}
