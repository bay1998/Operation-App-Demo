### Resource

#### ALB(Application Load Balancer) とは

- AWS が提供するロードバランサー
  - アプリケーションレイヤーでの振り分けが可能
  - SSL/TLS の暗号化/複号化
  - AWS Cognito との連携(今回 Rails 側で連携スクリプトを実装したためお役御免にはなるが、将来的には...)

#### Setting

- `terraform/main.tf`での設定は以下の通りである。

```
module "lb" {
source = "./lb"
aws_vpc = module.network.aws_vpc
aws_public_subnet = module.network.aws_public_subnet
aws_public_subnet_dummy = module.network.aws_public_subnet_dummy
security_group = module.firewall.aws_lb
enable_deletion_protection = false
tags = var.project
}
```

| Key                        | Value                                                          |       |
| -------------------------- | -------------------------------------------------------------- | ----- |
| aws_vpc                    | network モジュールで設定したパブリックサブネット の VPC ID     |
| aws_public_subnet          | network モジュールで設定したパブリックサブネット の ID         |
| aws_public_subnet_dummy    | network モジュールで設定したダミー用パブリックサブネット の ID |
| security_group             | ALB に紐づくセキュリティグループ                               |
| enable_deletion_protection | リソースの削除保護                                             | false |
