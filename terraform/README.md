### Terraformモジュール

#### 目的

本リポジトリはIaCツールである**Terraform**を活用して、効率的な各AWSリソースの構築支援を目的とした。

##### IaCツールを利用する理由

システムにAmazon Web Serviceを採用した際に、複数のAWSリソースを結合して、一つのシステムを組み上げるケースが一般的である。これらのシステム構築事例において、AWSマネジメントコンソール上からのGUI操作を実施することが可能である。しかし、AWSリソースを構築する際に、複数のパラメータを設定する必要があ理、この場合における設定やリソースの状態をマネジメントコンソールのみで管理するには、追跡が煩雑となりうる課題が発生する。

そこでIaCツールを利用し、リソースの設定一覧の全てをコード化とバージョン管理ツールを用いた管理を実施することで、上記で述べた課題を解消できる。IaCの導入により、利用者によらない再現性が高いインフラ構築を可能とする利点も挙げられる。

##### モジュール管理

また、あらゆるプロジェクトに付随するAWSリソースを、簡易的に構築できる管理が重要とされる。そこで、各リソースに割り当てられる設定管理をモジュール化し、利用コストの省力化を実現できるようなリポジトリを目指したい。

#### Terraform

Terraformとは、[Hashicorp](https://www.terraform.io)が開発したHCL言語で実装されたIaCツールである。Terraformは、Terraformエンジンとサポートされるクラウドプラットフォーム間のインターフェースであるプロバイダを利用する。

##### 導入方法


```
# 1. IAMアカウントの作成とCredential情報の追加
- IAMアカウントを作成する。
- IAM Management Consoleを開く。
- アクセスキーを発行する。
- 取得されるアクセスキー（access_key）とシークレットキー（secret_key）を控える。

# 2. AWS CLIの導入
$ pip3 install awscli --upgrade

# 3. Terraformの導入
$ brew install terraform
$ terraform --version

# 4. バージョンマネージャー「tfenv」の導入
$ brew install tfenv

# 5. リポジトリの取得
$ git clone git@gitlab.com:bay1998/terraform-aws.git
$ cd terraform-aws

# 6. terraform.tfvarsの修正
$ touch terraform.tfvars
$ echo 'access_key  = "[access_key]"' >> terraform.tfvars
$ echo 'secret_key  = "[secret_key]"' >> terraform.tfvars

# 7. Terraformの実行
$ terraform init
$ terraform plan
$ terraform apply
```

#### ディレクトリ構成

##### tfstate
`terraform apply`を実行した時点で作成されるファイル。Terraformが管理する現在の状態を記録する。Terraformは、tfstateの状態とHCLで記述したファイルとの差分があれば、差分情報のみを抽出し、変更処理を実施する。

##### terraform.tfvars
Terraformでは、HCLファイル上にて`variable`変数を定義できる。この`variable`は、`.tfvars`の拡張子を持つ外部ファイルにて定義をした後に、HCLファイルから読み込み、変数として参照可能となる。本ファイルは、Credential情報を記載している為、git管理外のファイルである。

|  変数 |  用途 |
| ---- | ---- |
| access_key | AWSマネジメントコンソール上から確認できるAccess_key |
| secret_key | AWSマネジメントコンソール上から確認できるSecret_key |

##### provider.tf

Terraformが提供するAWSプロバイダの設定を定義を設定した。
本ファイルの設定情報は、全てのファイルに対して適用される。

|  変数 |  値 |
| ---- | ---- |
| terraform.required_providers.aws.source  |  hashicorp/aws   |
| terraform.required_providers.aws.version   |  ~> 4.38.0  |
| aws.region| ap-northeast-1 |
| aws.access_key | var.access_key （tfvarsで定義した情報を参照）|
| aws.secret_key | var.secret_key （tfvarsで定義した情報を参照）|
