output "aws_vpc" {
  value = aws_vpc.vpc_network
}

output "aws_public_subnet" {
  value = aws_subnet.public_subnet
}

output "aws_public_subnet_dummy" {
  value = aws_subnet.public_subnet_dummy
}

output "aws_private_subnet" {
  value = aws_subnet.private_subnet
}
