### Resource

#### VPC

##### aws_vpc

- AWS 上で定義された仮想ネットワーク空間。この仮想ネットワークを細分化して、さらに複数のサブネットに分割される。

| Key                  | Value          |                                                   |
| -------------------- | -------------- | ------------------------------------------------- |
| cidr_block           | 192.168.0.0/16 |                                                   |
| enable_dns_support   | true           | DNS サーバーによる名前解決を有効化                |
| enable_dns_hostnames | true           | VPC リソースにパブリック DNS ホスト名を割り当てる |

##### public_subnet

- 外部からインターネット経由でアクセス可能であるパブリックサブネット。ALB が配置されている。

| Key                     | Value           |                      |
| ----------------------- | --------------- | -------------------- |
| availability_zone       | ap-northeast-1a |                      |
| cidr_block              | 192.168.1.0/24  |                      |
| map_public_ip_on_launch | true            | Public IP の割り当て |

##### private_subnet

- 外部からインターネット経由でアクセス不可のプライベートサブネット。API サーバーや DB、Redis が配置されている。

| Key                     | Value           |                      |
| ----------------------- | --------------- | -------------------- |
| availability_zone       | ap-northeast-1a |                      |
| cidr_block              | 192.168.2.0/24  |                      |
| map_public_ip_on_launch | false           | Public IP の割り当て |

#####　 internet_gateway

- VPC とインターネットとの間の通信を可能にするゲートウェイでありる。

#####　 nat_gateway

-

##### public_routes

- 各サブネットに割り当てられているルートテーブル。パブリックサブネット`public_subnet`に割り当てられている。

##### public_igw_route

- ルートテーブルを構成するルーティング一行分を示す。
- デフォルトゲートウェイ（0.0.0.0/0）が指定された通信はインターネットゲートウェイへ転送される。

| Key                    | Value                               |                               |
| ---------------------- | ----------------------------------- | ----------------------------- |
| route_table_id         | `public_routes`                     |                               |
| gateway_id             | `internet_gateway`                  |                               |
| destination_cidr_block | 0.0.0.0/0（デフォルトゲートウェイ） | ルーティング先の ネットワーク |

##### public_route_table_association

- ルートテーブル`public_routes`とパブリックサブネット`public_subnet`を関連づけている設定。

| Key            | Value           |     |
| -------------- | --------------- | --- |
| route_table_id | `public_routes` |     |
| subnet_id      | `public_subnet` |     |

##### private_nat_route

- ルートテーブルを構成するルーティング一行分を示す。
- デフォルトゲートウェイ（0.0.0.0/0）が指定された通信はインターネットゲートウェイへ転送される。

| Key                    | Value                               |                               |
| ---------------------- | ----------------------------------- | ----------------------------- |
| route_table_id         | `private_routes`                    |                               |
| gateway_id             | `nat_gateway`                       |                               |
| destination_cidr_block | 0.0.0.0/0（デフォルトゲートウェイ） | ルーティング先の ネットワーク |

##### private_route_table_association

- ルートテーブル`private_routes`とプライベートサブネット`private_subnet`を関連づけている設定。

| Key            | Value            |     |
| -------------- | ---------------- | --- |
| route_table_id | `private_routes` |     |
| subnet_id      | `private_subnet` |     |
