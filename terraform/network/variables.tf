variable "tags" {}

variable "vpc_endpoints_security_groups" {
  type = string
}

variable "vpc_endpoints_interface" {
  type = list(string)
}
