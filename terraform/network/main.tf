resource "aws_vpc" "vpc_network" {
  cidr_block           = "192.168.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = var.tags
  }
}

# Subnet
resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.vpc_network.id
  availability_zone       = "ap-northeast-1a"
  cidr_block              = "192.168.1.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name = var.tags
  }
}

resource "aws_subnet" "public_subnet_dummy" {
  vpc_id                  = aws_vpc.vpc_network.id
  availability_zone       = "ap-northeast-1c"
  cidr_block              = "192.168.3.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name = var.tags
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id                  = aws_vpc.vpc_network.id
  availability_zone       = "ap-northeast-1a"
  cidr_block              = "192.168.2.0/24"
  map_public_ip_on_launch = false

  tags = {
    Name = var.tags
  }
}

# Gateway
resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc_network.id

  tags = {
    Name = var.tags
  }
}

resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.internet_gateway]
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet.id
  depends_on    = [aws_internet_gateway.internet_gateway]
}

# PublicSubnet Routing Setting
resource "aws_route_table" "public_routes" {
  vpc_id = aws_vpc.vpc_network.id

  tags = {
    Name = var.tags
  }
}

resource "aws_route" "public_igw_route" {
  route_table_id              = aws_route_table.public_routes.id
  gateway_id                  = aws_internet_gateway.internet_gateway.id
  destination_ipv6_cidr_block = "::/0"
}

resource "aws_route_table_association" "public_route_table_association" {
  route_table_id = aws_route_table.public_routes.id
  subnet_id      = aws_subnet.public_subnet.id
}

# PrivateSubnet Routing Setting
resource "aws_route_table" "private_routes" {
  vpc_id = aws_vpc.vpc_network.id

  tags = {
    Name = var.tags
  }
}

resource "aws_route" "private_nat_route" {
  route_table_id         = aws_route_table.private_routes.id
  nat_gateway_id         = aws_nat_gateway.nat_gateway.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "private_route_table_association" {
  route_table_id = aws_route_table.private_routes.id
  subnet_id      = aws_subnet.private_subnet.id
}

# VPC Endpoint
resource "aws_vpc_endpoint" "interface_endpoint" {
  vpc_id            = aws_vpc.vpc_network.id
  for_each          = toset(var.vpc_endpoints_interface)
  service_name      = "com.amazonaws.ap-northeast-1.${each.value}"
  vpc_endpoint_type = "Interface"

  security_group_ids = [var.vpc_endpoints_security_groups]
  subnet_ids         = [aws_subnet.private_subnet.id]
  depends_on = [
    aws_vpc.vpc_network, aws_subnet.private_subnet
  ]

  private_dns_enabled = true

  tags = {
    Name = var.tags
  }
}
