variable "tags" {}

variable "nginx_name" {
  type = string
}

variable "nginx_port" {
  type = number
}

variable "nginx_image" {
  type = string
}

variable "nginx_security_groups" {
  type = list(string)
}

variable "subnet" {
  type = string
}

variable "iam_role" {
  type = string
}
