### Resource

#### ECS(Elastic Container Service)とは

- Docker コンテナのデプロイや運用管理を行うための、フルマネージドなコンテナオーケストレーションサービス

#### 運用上の注意

- ECS を活用して ECR に登録済みのコンテナイメージをもとに EC2 上にコンテナを稼働させる。
  - 一般的に Fargate を活用する方法もあるが、今回はコスト面を考慮し EC2 で実施した。

#### Setting

- `terraform/main.tf`での設定は以下の通りである。

```
module "ecs" {
  source                = "./ecs"
  nginx_name            = "nginx_container"
  nginx_port            = module.firewall.nginx_port
  nginx_image           = "${var.aws_id}.dkr.ecr.ap-northeast-1.amazonaws.com/${nginx_image_name}:0.1.0"
  nginx_security_groups = [module.firewall.aws_nginx_ec2, module.firewall.aws_ssh_ec2]
  subnet                = module.network.aws_private_subnet
  tags                  = var.project
}
```

| Key                   | Value                                                                                     |     |
| --------------------- | ----------------------------------------------------------------------------------------- | --- |
| nginx_name            | なんでも良い                                                                              |
| nginx_port            | Nginx コンテナ に対して ALB からの通信で許可しているポート                                |
| nginx_image           | Nginx 用コンテナイメージ名（`docker/web/Dockerfile`をもとに作成された）                   |
| nginx_security_groups | firewall モジュールで設定した Nginx 用コンテナに対して割り当ててるセキュリティグループ ID |
| subnet                | network モジュールで設定したプライベートサブネット の ID                                  |
