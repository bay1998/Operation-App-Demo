resource "aws_ecs_cluster" "ecs_cluster" {
  name = "ecs-cluster"

  tags = {
    Name = var.tags
  }
}

resource "aws_ecs_task_definition" "ecs_task" {
  requires_compatibilities = ["FARGATE"]
  family                   = "task-${var.tags}"
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512

  execution_role_arn = var.iam_role

  container_definitions = <<DEFINITION
[
    {
      "name"      : "${var.nginx_name}",
      "image"     : "${var.nginx_image}",
      "cpu"       : 256,
      "memory"    : 512,
      "essential" : true,
      "networkMode": "awsvpc",
      "portMappings" : [
        {
          "containerPort" : ${var.nginx_port},
          "hostPort"      : ${var.nginx_port}
       }
    ]
  }
]
DEFINITION

  tags = {
    Name = var.tags
  }
}

resource "aws_ecs_service" "my_service" {
  name            = "my-service"
  cluster         = aws_ecs_cluster.ecs_cluster.id
  task_definition = aws_ecs_task_definition.ecs_task.arn
  launch_type     = "FARGATE"
  desired_count   = 1

  network_configuration {
    subnets          = [var.subnet]
    security_groups  = var.nginx_security_groups
    assign_public_ip = false
  }

  tags = {
    Name = var.tags
  }
}
