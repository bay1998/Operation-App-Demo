## README

#### 目的

- 最低限の機能だけのアプリケーションを作成して、数ヶ月程度、本番環境で運用するところまでをゴールとします。

#### リポジトリ名

- AWS Cognito を用いてユーザー認証を可能にするアプリケーション。
  - ユーザー認証機能： `済`
- 将来的には、さらに機能を拡張して、サーバー作業の手順を管理するアプリケーションを作成したい。

#### バージョン

- API
  - `Ruby`: `3.1.2`
  - `Rails`: `7.0.5`
  - `Puma`: `5.0`
- DB
  - `MySQL`: `8.0.33`
- Redis
  - `Redis`: `7.0.12`
- Web
  - `nginx`: `1.25.2`

#### 環境構築

1.  本リポジトリを以下のコマンドで取得します。

```
git clone https://gitlab.com/bay1998/Operation-App-Demo.git
```

2.  AWS CLI のインストールから Terraform の実行までは[こちら](https://gitlab.com/bay1998/Operation-App-Demo/-/tree/main/terraform?ref_type=heads)の手順をもとに完了してください。

3.  2 の作業がすると、環境変数の設定をしてください。

- DB,Redis に関する設定を決定してください。
- [AWS Cognito](https://aws.amazon.com/jp/cognito/)へログインしていただき、
- `cd docker && cp template.env .env` を実行してください。
- `vim .env` を実行して、.env 以下の必要情報を入力してください。

```
DB_USER=(DB 一般権限ユーザー)
DB_NAME=(DB名)
DB_PASSWORD=(DB_USERのパスワード)
DB_ROOT_PASSWORD=(DB 管理者ユーザーのパスワード)
REDIS_URL=(REDISへのアクセスURL)
REDIS_USER=(REDIS 一般権限ユーザー)
REDIS_PASSWORD=(REDIS_USERのパスワード)
AWS_ACCESS_KEY_ID=(AWSCLIで使用するキーID)
AWS_SECRET_ACCESS_KEY=(AWSCLIで使用するシークレットキー)
AWS_COGNITO_APP_CLIENT_ID=(COGNITOで登録したアプリケーションクライアントのID)
AWS_COGNITO_APP_CLIENT_SECRET=(COGNITOで登録したアプリケーションクライアントのシークレットキー)
AWS_COGNITO_DOMAIN=(COGNITOで登録したドメイン)
AWS_COGNITO_POOL_ID=(COGNITOで登録したユーザープールのID)
AWS_COGNITO_REGION=(COGNITOで設定されたリージョン)
```

4. 上記のすべての情報を入力し終えると、以下のコマンドでコンテナを起動してください。

```
cd docker && docker build .
docker-compose up -d
```

5. ホスト上で http://localhost:8080 へアクセスすると、アプリケーションが表示されます。

#### 開発方法

- http://bay1998.net/projects/operation-app-demo
  - 機能設計やアプリケーションに関する考察は上記のチケット管理ツールにて管理しています。
