class AuthController < ApplicationController

    def signin
        unless params[:code]
            Rails.logger.error("You cannot log in because the code does not exist. It's an invalid request.")
            render :nothing => true, :status => :bad_request and return
        end

        resp = looked_auth_code(params[:code])
        unless resp
            Rails.logger.info("Unable to log in. It is presumed that authorization has failed in Cognito. : #{params[:code]}")
            redirect_to '/' and return
        end

        ActiveRecord::Base.transaction do
            @current_user = User.where(subscriber: resp.id_token[:sub]).first
            if @current_user.nil?
                @current_user = User.create(subscriber: resp.id_token[:sub],
                                   email: resp.id_token[:email],
                                   username: resp.id_token["cognito:username"])
            end
            session[:session_data] = resp.id_token
            Rails.logger.info("You have successfully logged in. Log-in user: #{@current_user.subscriber}") if @current_user
            redirect_to '/home'
        end
    end

    def signout
        session.delete(:session_data) if session[:session_data]
        Rails.logger.info("You have successfully logged out.")
        redirect_to '/'
    end

    private

    def looked_auth_code(code)
        client = CognitoClient.new(:redirect_uri => auth_sign_in_url)
        client.get_pool_tokens(code)
    end

end
