class HomeController < ApplicationController

    def top
        unless @current_user.nil?
            Rails.logger.info("Since a logged-in user (#{@current_user.subscriber}) exists, you will be redirected to the HOME page (/home).")
            redirect_to '/home'
        end 
    end

    def show
        if @current_user.nil?
            Rails.logger.info("Since there is no logged-in user, you will be redirected to the homepage (/).")
            redirect_to '/'
        end 
    end

end
