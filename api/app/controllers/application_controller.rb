require 'cognito_jwt_keys'
require 'cognito_client'

class ApplicationController < ActionController::Base

    before_action :logged_in_user
    @current_user = nil
    @session = nil

    def exist_session?
      @session = session[:session_data] ? session[:session_data] : nil
      Rails.logger.info("Found a cognito session: #{@session.to_s[0...10]}") if @session
      return @session
    end

    def valid_session?
      @session = session[:session_data][:exp] > Time.now.to_i ? session[:session_data] : able_refresh_token?
      Rails.logger.info("Found a non-expired cognito session: #{@session.to_s[0...10]}") if @session
      return @session
    end

    def able_refresh_token?
      Rails.logger.info("Found a refreshed cognito session expired: #{@session.to_s[0...10]}") if @session
      return refresh_cognito_session(session[:session_data])
    end

    def current_user
      @current_user = User.where(subscriber: @session[:sub]).first
      Rails.logger.info("Found a log-in user: #{@current_user.subscriber}") if @current_user
    end

    def logged_in_user
      current_user if exist_session? && valid_session?
    end

  private

    def refresh_cognito_session(session_data)
      client = CognitoClient.new(:redirect_uri => auth_sign_in_url)
      resp = client.refresh_id_token(session_data[:refresh_token])
      Rails.logger.info("id-token refreshed")

      return nil unless resp
      session_data = resp.id_token
      @session = session_data
      return session_data
    end

end
