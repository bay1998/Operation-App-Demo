### アプリケーションに関する説明

#### 実装した機能

実装済みのアプリケーションの機能については、以下の表にて、追記します。
Version0.1 の段階で運用を開始し、機能の実装が完了次第、追加でリリースしていく運用を目指します。

| Version | 追加機能                         |
| ------- | -------------------------------- |
| 0.1     | ログイン/新規登録/ログアウト処理 |
| 1.0     | comming soon ...                 |

現在のリリースバージョンは、 **_0.1_** です。

#### ログイン処理について

##### 前提知識

| 用語                                                                                                                                 | 説明                                                                                                                                                                                                                         |
| ------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| AWS Cognito                                                                                                                          | ID やアクセスを管理する AWS のサービス。                                                                                                                                                                                     |
| Session                                                                                                                              | クライアントとサーバー間での接続状態やデータを保存する仕組み。                                                                                                                                                               |
| UserPool                                                                                                                             | AWS 内での認証を扱うサービス。認証を完了したユーザーが保存されていく。                                                                                                                                                       |
| JWT(JSON Web Token)                                                                                                                  | JSON 形式で表現された認証情報を共有するために、適用される標準規格。                                                                                                                                                          |
| [ID Token](https://docs.aws.amazon.com/ja_jp/cognito/latest/developerguide/amazon-cognito-user-pools-using-the-id-token.html)        | name、email、および phonenumber など、認証されたユーザーのアイデンティティに関するクレームが含まれる JSON 形式の Web トークン。ID トークンの有効期限は、**60 分** に設定していますが、5 分〜1 日の範囲までなら設定可能です。 |
| [更新 Token](https://docs.aws.amazon.com/ja_jp/cognito/latest/developerguide/amazon-cognito-user-pools-using-the-refresh-token.html) | ID トークンを再発行するために使用されるトークン。更新 トークンの有効期限は、**30 日** に設定しており、更新トークンの期限を超えた場合、セキュリティ安全性の観点により、再ログインを必須とする。                               |

##### ログイン処理に関連するインフラのシーケンス図

ログイン処理についてのフローが複雑であり、第三者が本コードを把握するにあたり、困難を極める可能性があります。そこで、システムの概要・仕様・処理の流れをを視覚的に表現するシーケンス図を用いて作成しました。

| サーバー           | 説明                                                                                                                                   |
| ------------------ | -------------------------------------------------------------------------------------------------------------------------------------- |
| Application(Rails) | Rails が動作するサーバー。クライアントからのリクエスストを受け取り、リダイレクト処理・Redis や DB へのデータの保存処理などを実行する。 |
| Redis              | ID トークン情報やリフレッシュトークンの保存先として活用する。                                                                          |
| DB                 | アプリケーションで参照更新するデータの保存先として活用する。                                                                           |
| 認証サーバー       | リダイレクトにより、Application の代わりにユーザーの認証機能を担当するサーバー。                                                       |

<img src="./doc/Login-sequence.drawio.png" alt="ログイン処理" title="ログイン処理">

##### ログイン処理に関連するアプリケーション・アクションのシーケンス図

コード内のクラス・アクションのフローが複雑であり、第三者が本コードを把握するにあたり、困難を極める可能性があります。そこで、ログイン時と ID トークンの再取得時のアクションにおけるフローを視覚的に表現するシーケンス図を用いて作成しました。

| Class                  | 説明                                                                                                                                                                       |
| ---------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Application            | アクセス時に実行される共通コントローラ。ログインユーザー情報を保持する。                                                                                                   |
| Session                | AWS Cognito での認証を実現するために、リダイレクトが定義されたクラス。                                                                                                     |
| CognitoUrls            | AWS Cognito へリダイレクト・アクセス用 URL を作成するクラス。                                                                                                              |
| Auth                   | AWS Cognito での認証後、コールバックされた後に実行されるアクションが定義されているクラス。アプリ側でセッションやユーザーデータを、Redis や DB へ保存する処理が実行される。 |
| CognitoClients         | ID トークンの取得や 更新 トークンを用いて ID トークンを更新したい場合のリクエストを、送信するアクションが定義されるクラス。                                                |
| CognitoJwtKeysProvider | TBD                                                                                                                                                                        |
| CognitoJwtKeys         | TBD                                                                                                                                                                        |
| CognitoPoolTokens      | 取得した ID トークンの展開処理を実行するクラス。                                                                                                                           |

<img src="./doc/Class-sequence.drawio.png" alt="ログイン処理" title="ログイン処理">

#### クラス図

#### デプロイ方法
