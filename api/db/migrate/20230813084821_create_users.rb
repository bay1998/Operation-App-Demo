class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users, id: false do |t|
      t.column :subscriber, :string, primary_key: true
      t.column :username, :string, limit:30, null: false
      t.column :email, :string, limit:50, null: false
      t.timestamps
    end
  end
end
