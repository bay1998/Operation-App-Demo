class DropSessions < ActiveRecord::Migration[7.0]
  def change
    drop_table :sessions do |t|

      t.timestamps
    end
  end
end
