class CreateOperations < ActiveRecord::Migration[7.0]
  def change
    create_table :operations do |t|
      t.string :title
      t.datetime :date
      t.string :block
      t.string :status

      t.timestamps
    end
  end
end
