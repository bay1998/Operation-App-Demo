class DropOperations < ActiveRecord::Migration[7.0]
  def change
    drop_table "operations", charset: "utf8mb3", force: :cascade do |t|
      t.string "title"
      t.datetime "date"
      t.string "block"
      t.string "status"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
  end
end
