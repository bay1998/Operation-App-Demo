Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  root to: "home#top"
  get '/sign_up', as: 'signup', to: 'sessions#signup'
  get '/sign_out', as: 'signout', to: 'sessions#signout'
  get '/sign_in', as: 'signin', to: 'sessions#signin'
  get 'auth/sign_in', to: 'auth#signin'
  get 'auth/sign_out', to: 'auth#signout'
  get '/home', to: 'home#show'
end
