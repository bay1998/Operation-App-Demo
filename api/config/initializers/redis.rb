path = Rails.root + 'config/redis.yml'
yaml = Pathname.new path if path
prod =  Rails.env.production?

if yaml && yaml.exist?
  @config = YAML.respond_to?(:safe_load) ? 
  YAML.safe_load(ERB.new(yaml.read).result, aliases: true)[Rails.env] : 
  YAML.load(ERB.new(yaml.read).result)[Rails.env] || {}
else
  raise "Could not load redis configuration. No such file - #{path}"
end

Rails.application.config.session_store :redis_store,
  servers:  [ { host: @config["host"], port: @config["port"], db: 1, password: @config["password"] }, ],
  expire_after: 90.minutes,
  key: "_#{Rails.application.class.module_parent_name.downcase}_session",
  threadsafe: prod,
  secure: prod